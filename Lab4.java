package lab4;
//test commit
import java.util.Random;

class Order {
	String lastname;
	int places;
	int duration;
	
	public Order(String lastname, int places, int duration) {
		this.lastname = lastname;
		this.places = places;
		this.duration = duration;
	}

	public String toString() {
		return lastname + "; " + Integer.toString(places)+ "; " + Integer.toString(duration);
	}
}

class Room {
	private boolean free = true;
	private int places;
	
	Room(int number, int places) {
		this.places = places;
	}
	
	public void takeOrder(Order order) {
		if (canTakeOrder(order)) {
			free = false;
		}
		else
			System.out.println("Can not take order: "+order.toString());	
	}
	
	public void deleteExistingOrder() {
		free = true;
	}
	
	public boolean canTakeOrder(Order order) {
		return (free && order.places <= places);
	}
}

class OrderManager implements Runnable {
	
	Thread thread;
	private Order order;
	private Hotel hotel;
	private int roomNumber = -1;
	private int orderNumber;
	
	OrderManager(int number, Order order, Hotel hotel) {
		this.order = order;
		this.hotel = hotel;
		this.orderNumber = number;
		System.out.println("������ ����� �" + number + " (" + order.toString()+")");
		thread = new Thread(this, Integer.toString(orderNumber));
		thread.start();
	}

	@Override
	public void run() {
		System.out.println("������� ����� ������� ��� ������ �" + orderNumber + "...");
		do {
			roomNumber = hotel.getNumberOfAvailableRoomForOrder(order);
		}
		while (roomNumber == -1);
		
		if (roomNumber != -1) {
			System.out.println("����� �" + orderNumber + " ������� �������! ����� ����������: " + order.duration);
			
			try {
				Thread.sleep((long) order.duration * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("����� �"+orderNumber+" �������� �������");
			hotel.leaveRoom(roomNumber);
			//notify();
		}
	}
}

class Hotel {
	private int count = 10;
	private Room rooms[] = null;
	
	Hotel(int roomsCount) {
		count = roomsCount;
		rooms = new Room[count];
		for (int i = 0, p = 4; i<rooms.length; ++i, p = p<4?p+1:1) {
			int places = p;
			rooms[i] = new Room(i, places);
		}
	}
	
	synchronized public int getNumberOfAvailableRoomForOrder(Order order) {
		for (int i = 0; i<count; ++i) {
			if (rooms[i].canTakeOrder(order)) {
				rooms[i].takeOrder(order);
				return i;
			}
		}
		return -1;
	}
	
	synchronized public void leaveRoom(int number) {
		if (number<count)
			rooms[number].deleteExistingOrder();
		else
			System.out.println("Wrong room number!");
	}
	
}

public class Lab4 {

	public static void main(String[] args) {
		Hotel hotel = new Hotel(5);
		
		int count = 25;
		
		OrderManager threads[] = new OrderManager[count];
		Random rand = new Random();
		
		for (int i=0;	i<count;	++i) {
			
			int places = Math.abs(rand.nextInt()%4)+1;
			int duration = Math.abs(rand.nextInt()%10)+1;
			
			threads[i] = new OrderManager(i, new Order("Order" + Integer.toString(i+1), places, duration), hotel);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for (int i=0;	i<count;	++i)
			try {
				threads[i].thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		System.out.println("���.");

	}

}
